---
title: "Quasi fatto..."
type: page
layout: subpage
---

## Controlla ora la tua casella di posta elettronica

Hai quasi finito! Riceverai presto una email con un link per la conferma finale della tua firma. Verifica la tua cartella di spam se non ricevi l'email nei prossimi 15 minuti. 

Grazie per aver firmato la lettera aperta per **Public Money, Public Code**.
